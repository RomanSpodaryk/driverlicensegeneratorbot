package com.botscrew.models;

public enum DriverCategory {
    A1,
    A,
    B1,
    B,
    C,
    D,
    E
}
