package com.botscrew.models;

public enum Gender {
    MALE,
    FEMALE
}
